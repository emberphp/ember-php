DS.Model.extend({
	comments: DS.hasMany('comment', {
		async: true
	}),
	post: DS.belongsTo('post', {
		async: true
	})
});
