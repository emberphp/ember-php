<?php
namespace DS;
/**
	* The model class that all Ember Data records descend from.
	* https://github.com/emberjs/data/pull/958
	*/
class ValidationError extends \Exception {
	private $modelProperty;
	
	public function __construct($modelProperty, $message) {
		parent::__construct($message, 422);
		
		$this->modelProperty = $modelProperty;
	}
	
	public function getModelProperty() {
		return $this->modelProperty;
	}
}

?>