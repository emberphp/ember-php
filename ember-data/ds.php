<?php
/**
	* All Ember Data methods and functions are defined inside of this namespace.
	*/
namespace DS;

/**
	```php
	DS\parseJSModel("DS.Model.extend({
		firstName: DS.attr(),

		lastName: DS.attr(),


			 birthday:    DS.attr(),

		fullName: function() {
			return this.get('firstName') + ' ' + this.get('lastName');
		}.property('firstName', 'lastName') ,

		birthday: DS.attr('date'),

		username: DS.attr('string'),
				email: DS.attr('string'),
				verified: DS.attr('boolean', {defaultValue: false}),
				createdAt: DS.attr('string', {          defaultValue: function() { return new Date(); }  , async : true      })

				,

				profile: DS.belongsTo('profile'),

				user: DS.belongsTo('user'),

				comments: DS.hasMany('comment'),


				post: DS.belongsTo('post'),

				tags: DS.hasMany('tag'),


				posts: DS.hasMany('post'),

				onePost: DS.belongsTo('post'),
		twoPost: DS.belongsTo('post'),
		redPost: DS.belongsTo('post'),
		bluePost: DS.belongsTo('post'),

		comments: DS.hasMany('comment', {    inverse: 'redPost'  , async : true  })

	});");

	> array(17) {
		["firstName"]=>
		array(2) {
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(6) "string"
		}
		["lastName"]=>
		array(2) {
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(6) "string"
		}
		["birthday"]=>
		array(2) {
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(4) "date"
		}
		["username"]=>
		array(2) {
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(6) "string"
		}
		["email"]=>
		array(2) {
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(6) "string"
		}
		["verified"]=>
		array(3) {
			["props"]=>
			array(1) {
				["defaultValue"]=>
				string(5) "false"
			}
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(7) "boolean"
		}
		["createdAt"]=>
		array(3) {
			["props"]=>
			array(2) {
				["defaultValue"]=>
				string(33) "function() { return new Date(); }"
				["async"]=>
				string(4) "true"
			}
			["type"]=>
			string(4) "attr"
			["attt"]=>
			string(6) "string"
		}
		["profile"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(7) "profile"
		}
		["user"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "user"
		}
		["comments"]=>
		array(3) {
			["props"]=>
			array(2) {
				["inverse"]=>
				string(7) "redPost"
				["async"]=>
				string(4) "true"
			}
			["type"]=>
			string(4) "many"
			["attt"]=>
			string(7) "comment"
		}
		["post"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "post"
		}
		["tags"]=>
		array(2) {
			["type"]=>
			string(4) "many"
			["attt"]=>
			string(3) "tag"
		}
		["posts"]=>
		array(2) {
			["type"]=>
			string(4) "many"
			["attt"]=>
			string(4) "post"
		}
		["onePost"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "post"
		}
		["twoPost"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "post"
		}
		["redPost"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "post"
		}
		["bluePost"]=>
		array(2) {
			["type"]=>
			string(6) "belong"
			["attt"]=>
			string(4) "post"
		}
	}
	```
	*/
function parseJSModel($s) {
	$s = trim(preg_replace('/\s\s+/', ' ', $s));
	$parseObj = function($obj) {
		$tt = [];
		$line = trim(preg_replace('!\s+!', ' ', $obj), ' ');
		array_walk(array_map(function($v) {
			return trim($v);
		}, explode(',', trim($line, '{}'))), function($v) use(&$tt) {
			list($prop, $val) = explode(':', $v, 2);
			$tt[trim($prop)] = trim($val, " '");
		});
		return $tt;
	};

	$man = [];

	$fon1 = 'DS.Model.extend({';
	$fon2 = '});';

	$start = stripos($s, $fon1);
	$end = strripos($s, $fon2);
	if($start !== false && $end !== false) {
		$js = substr($s, $start + strlen($fon1), $end - strlen($fon1));
		$js_stripped_new_lines = preg_replace('/^[ \t]*[\r\n]+/m', '', $js);
		array_walk(preg_split('/$\R?^/m', $js_stripped_new_lines), function($v) use(&$man, &$parseObj) {
			$line = trim(preg_replace('!\s+!', ' ', $v), ' ');
			if(stripos($line, ':') !== false) {
				list($propName, $fn) = explode(':', $line, 2);
				$fn = trim($fn, ', ');
				if(stripos($fn, 'DS.') === 0) {
					$parts = [];
					if(stripos($fn, 'DS.attr(') === 0) {
						$t = substr($fn, 8, strlen($fn) - 9);
						if(stripos($t, ',') !== false) {
							list($type, $props) = explode(',', $t, 2);
							$parts['props'] = $parseObj($props);
						} else {
							$type = $t;
						}
						$parts['type'] = 'attr';
						$parts['attt'] = trim((empty($type)? 'string': $type), " '");
					} elseif(stripos($fn, 'DS.belongsTo(') === 0) {
						$t = substr($fn, 13, strlen($fn) - 14);
						if(stripos($t, ',') !== false) {
							list($type, $props) = explode(',', $t, 2);
							$parts['props'] = $parseObj($props);
						} else {
							$type = $t;
						}
						$parts['type'] = 'belong';
						$parts['attt'] = trim($type, " '");
					} elseif(stripos($fn, 'DS.hasMany(') === 0) {
						$t = substr($fn, 11, strlen($fn) - 12);
						if(stripos($t, ',') !== false) {
							list($type, $props) = explode(',', $t, 2);
							$parts['props'] = $parseObj($props);
						} else {
							$type = $t;
						}
						$parts['type'] = 'many';
						$parts['attt'] = trim($type, " '");
					}
					$man[$propName] = $parts;
				}
			}
		});
	}

	return $man;
}

function saveModelsManifest($dir) {
	$files = [];
	foreach((new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir))) as $filename => $file) {
		if($file->isDir() || $file->getExtension() != 'js') {
			continue;
		}
		$files[\Ember\String\dasherize($file->getBasename('.js'))] = parseJSModel(file_get_contents($filename));
	}
	return json_encode($files);
}

function version() {
	return '1.0.1';
}

/**
  `DS.attr` defines an attribute on a DS.Model.
  By default, attributes are passed through as-is, however you can specify an
  optional type to have the value automatically transformed.
  Ember Data ships with four basic transform types:
    'string', 'number', 'boolean' and 'date'.
  You can define your own transforms by subclassing DS.Transform.

  DS.attr takes an optional hash as a second parameter, currently
  supported options are:
    'defaultValue': Pass a string or a function to be called to set the attribute
                    to a default value if none is supplied.

  @method attr
  @param {String} type the attribute type
  @param {Object} options a hash of options
	*/
function attr($type, $options = array()) {
	$transformTypes = array('string', 'number', 'boolean', 'date');
	//return DS\Model::attr($type, $options);
	
	if($type == 'string') {
		$transform = new StringTransform;
	} elseif($type == 'number') {
		$transform = new NumberTransform;
	} elseif($type == 'boolean') {
		$transform = new BoolTransform;
	} elseif($type == 'date') {
		$transform = new DateTransform;
	}
	
	if(array_key_exists('db', $options) && strlen($options['db']) > 0) {
		$transform->db = $options['db'];
	}
	
	$transform->noupdatecreate = (array_key_exists('noupdatecreate', $options) && $options['noupdatecreate'] == true);
	
	foreach(array('defaultValue', 'min', 'max', 'minMsg', 'maxMsg') as $key) {
		if(array_key_exists($key, $options) && $options[$key] != NULL) {
			$transform->{$key} = $options[$key];
		}
	}
	
	return $transform;
}

function hasMany($type, $options = array()) {
	$relationship = new HasManyRelationship;
	
	$relationship->type = $type;
	
	if(array_key_exists('db', $options) && strlen($options['db']) > 0) {
		$relationship->db = $options['db'];
	}
	
	if(array_key_exists('embedded', $options) && strlen($options['embedded']) > 0) {
		$relationship->embedded = $options['embedded'];
	}
	
	return $relationship;
}

function belongsTo($type, $options = array()) {
	$relationship = new BelongsToRelationship;
	
	$relationship->type = $type;
	
	if(array_key_exists('db', $options) && strlen($options['db']) > 0) {
		$relationship->db = $options['db'];
	}
	
	return $relationship;
}

?>
