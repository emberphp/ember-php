<?php
namespace DS;

class Transform {
	public $type, $defaultValue,
	
	$db, $noupdatecreate, $min, $minMsg, $max, $maxMsg;
	
	public function type() {
		return $type;
	}
	
	public function serialize($value) {
		
	}
	
	public function deserialize($value) {
		
	}
}

class StringTransform extends Transform {
	public function serialize($value) {
		return $value;
	}
	public function deserialize($value) {
		return (string)$value;
	}
	
	public function __construct() {
		$this->type = 'string';
	}
}

class DateTransform extends Transform {
	public function serialize($value) {
		if(substr($value, 0, 4) == '0000') {
			return NULL;
		}
		return str_replace(' ', 'T', $value).'Z';
	}
	
	/**
		Use NULL for a MySQL 0000-00-00 00:00:00 on the database
		All other dates are moved as-is
		*/
	public function deserialize($value) {
		try {
			if(empty($value)) {
				return '0000-00-00 00:00:00';
			} else {
				$newValue = new \DateTime($value);
				return $newValue->format('Y-m-d H:i:s');
			}
		} catch(\Exception $e) {
			return '0000-00-00 00:00:00';
		}
	}
	
	public function __construct() {
		$this->type = 'date';
	}
}

class BoolTransform extends Transform {
	public function serialize($value) {
		return ($value > 0? true: false);
	}
	public function deserialize($value) {
		return ($value? 1: 0);
	}
	
	public function __construct() {
		$this->type = 'boolean';
	}
}

class NumberTransform extends Transform {
	public function serialize($value) {
		if(\is_numeric($value)) {
			if($value == (string)(float)$value) {
				return \floatval($value);
			} elseif($value == (string)(int)$value) {
				return \intval($value);
			}
		} else {
			return 0;
		}
	}
	public function deserialize($value) {
		if(\is_numeric($value)) {
			if($value == (string)(float)$value) {
				return \floatval($value);
			} elseif($value == (string)(int)$value) {
				return \intval($value);
			}
		} else {
			return 0;
		}
	}
	
	public function __construct() {
		$this->type = 'number';
	}
}

?>