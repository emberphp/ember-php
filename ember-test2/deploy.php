<?php
namespace Ember\Test;
/**
	Provides a base layer for deploying an Ember App.

	``` html ````
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Ember Starter Kit</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
  <script type="text/x-handlebars">
    <h2>Welcome to Ember.js</h2>

    {{outlet}}
  </script>

  <script type="text/x-handlebars" id="index">
    <ul>
    {{#each item in model}}
      <li>{{item}}</li>
    {{/each}}
    </ul>
  </script>

  <script src="js/libs/jquery-1.10.2.js"></script>
  <script src="js/libs/ember-template-compiler-1.10.0.js"></script>
  <script src="js/libs/ember-1.10.0.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
	``` html ````


	``` javascript ```
App = Ember.Application.create();

App.Router.map(function() {
  // put your routes here
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return ['red', 'yellow', 'blue'];
  }
});
	``` javascript ```


	``` io ```
	assets/
	javascripts/
		|_ components/
		|_ controllers/
		|_ helpers/
		|_ lib/
		|_ mixins/
		|_ models/
		|_ routes/
		|_ vendor/  -- ALREADY MINIFIED LIBS
		|_ views/
		|_ app.js
		|_ file1.js
		|_ file2.js
		|_ filen...n.js
	stylesheets/
		|_file1.css
		|_file2.css
		|_filen...n.css
	``` io ```


	``` usage ```
	> Ember\Test\Deploy::init('App', '/path/to/minified/js/libs/', '/path/to/javascripts/', '/path/to/stylesheets/', array(
	> 	'libs' => array(
	>			'/path/to/js/lib1.js',
	>			'/path/to/js/lib2.js',
	>		),
	> 	'jslibs' => array(
	>			'/path/to/javascripts/lib/lib1.js',
	>			'/path/to/javascripts/lib/lib2.js',
	>		),
	> 	'stylesheets' => array(
	>			'/path/to/js/lib1.js',
	>			'/path/to/js/lib2.js',
	>		),
	> ));
	> $fileContents = Ember\Test\Deploy::js();
	> var_dump(
	> 	first($fileContents), // /path/to/minified/js/libs/*.js + /path/to/javascripts/vendor/*.min.js + /path/to/javascripts/*.js
	> 	end($fileContents)
	> );
	> $fileContents = Ember\Test\Deploy::css();
	> var_dump($fileContents);
	> Ember\Test\Deploy::assets('<container-name>');
	``` usage ```

	*/
final class Deploy {
	protected static $app;
	protected static $libs;
	protected static $js;
	protected static $css;
	protected static $priorities;
	protected static $assets;

	public static function init($app, $libs, $js, $css, $priorities, $assets) {
		self::$app = $app;
		self::$libs = $libs;
		self::$js = $js;
		self::$css = $css;
		self::$priorities = $priorities;
		self::$assets = $assets;
	}

	public static function js() {
		$app = self::$app;
		// js
		$fileContents = '';
		$allPaths = array_merge(
			glob(self::$libs.'*.js'),
			glob(self::$js.'vendor/*.js'),
			glob(self::$js.'*.js')
		);
		$priorityPaths = (array_key_exists('libs', self::$priorities)? self::$priorities['libs']: array());
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents) {
			$fileContents .= file_get_contents($file);
		});
		$fileContents1 = $fileContents;
		$fileContents = '';
		$allPaths = glob(self::$js.'lib/*.js');
		$priorityPaths = (array_key_exists('jslibs', self::$priorities)? self::$priorities['jslibs']: array());
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents, &$app) {
			if(strrpos(basename($file), $app.'.', -strlen(basename($file))) !== false) {
				$fileContents .= basename($file, '.js').'='.file_get_contents($file);
			} else {
				$fileContents .= file_get_contents($file);
			}
		});
		$GLOBALS[self::$app] = new \Ember\Application;
		array_walk(glob(self::$js.'models/*.php'), function($file, $index) use(&$fileContents) {
			require_once $file;
		});
		$fileContents .= $GLOBALS[self::$app]->toJavascriptModel();
		array_walk(glob(self::$js.'models/*.js'), function($file, $index) use(&$fileContents) {
			$fileContents .= file_get_contents($file);
		});
		array_walk(glob(self::$js.'mixins/*.js'), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'=';
			$fileContents .= file_get_contents($file);
		});
		array_walk(glob(self::$js.'components/*.js'), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Component=';
			$fileContents .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'routes/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Route=';
			$fileContents .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'views/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'View=';
			$fileContents .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'controllers/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Controller=';
			$fileContents .= file_get_contents($file);
		});
		array_walk(glob(self::$js.'helpers/*.js'), function($file, $index) use(&$fileContents, &$app) {
			$fileContents .= "\n\nEm.Handlebars.registerBoundHelper('".basename($file, '.js')."',";
			$fileContents .= file_get_contents($file);
			$fileContents .= ');';
		});

		return array($fileContents1, $fileContents);
	}

	public static function css() {
		$fileContents = '';
		$allPaths = array_merge(
			glob(self::$css.'*.css')
		);
		$priorityPaths = (array_key_exists('libs', self::$priorities)? self::$priorities['stylesheets']: array());
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$fileContents) {
			$fileContents .= file_get_contents($file);
		});
		return $fileContents;
	}

	public static function assets() {
		$files = [];
		foreach((new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(self::$assets))) as $filename => $file) {
			if($file->isDir() || $file->getExtension() == 'php') {
				continue;
			}
			$fInfo = (new \finfo(\FILEINFO_MIME_TYPE));
			$file->azureContentType = $fInfo->file($file->getPathName());
			$files[] = $file;
		}
		return $files;
	}
}

?>
