<?php

require_once 'ember-runtime/ember.string.php';

array_walk(array_merge(
	glob(dirname(realpath(__FILE__)).'/ember-runtime/*.php'),
	glob(dirname(realpath(__FILE__)).'/ember-data/*.php'),
	glob(dirname(realpath(__FILE__)).'/ember-application/*.php'),
	glob(dirname(realpath(__FILE__)).'/broccoli-static-compiler/*.php'),
	glob(dirname(realpath(__FILE__)).'/ember-test2/*.php')
), function(&$file, $index) {
	require_once $file;
});

?>
