<?php
namespace Ember;
/**
	An instance of Ember\Application is the starting point for every Ember application. It helps to instantiate, initialize and coordinate the many objects that make up your app.
	*/
class Application {
	/**
		Location for overloaded data.
		Overloading not used on declared properties.
		Overloading only used on this when accessed outside the class.
	*/
	private $data = array();
	
	private $name, $autoload;
		
	public function __set($name, $value) {
		$this->data[$name] = $value;
	}

	public function __get($modelName) {
		if(!empty($this->autoload)) {
			@include_once $this->autoload.String\dasherize($modelName).'.php';
		}
		if(array_key_exists($modelName, $this->data)) {
			return $this->data[$modelName];
		}
		return NULL;
	}
	
	public function __isset($modelName) {
		return isset($this->data[$modelName]);
	}
	
	public function __unset($modelName) {
		unset($this->data[$modelName]);
	}
	
	function __construct($name = 'App', $autoload = NULL) {
		$this->name = $name;
		$this->autoload = $autoload;
	}
	
	function __toString() {
		return $this->name;
	}
	
	public function toJavascriptModel() {
		$js = '';
		foreach($this->data as $modelName => $model) {
			$js .= $this->name.'.'.String\classify($modelName).'=DS.Model.extend({';
			
			foreach($model->attributes as $key => $attribute) {
				if($key == 'id') {
					continue;
				}
				if($attribute instanceof \DS\BelongsToRelationship) {
					$js .= $key.":DS.belongsTo('".$attribute->type."'),";
				} elseif($attribute instanceof \DS\HasManyRelationship) {
					$js .= $key.":DS.hasMany('".$attribute->type."',{async:true".(!empty($attribute->embedded)? ",embedded:'".$attribute->embedded."'": '')."}),";
				} elseif($attribute instanceof \DS\Transform) {
					$js .= $key.":DS.attr(".($attribute->type? "'".$attribute->type."'": '').($attribute->defaultValue != NULL? ",{defaultValue:".($attribute instanceof \DS\StringTransform? "'": '').$attribute->deserialize($attribute->defaultValue).($attribute instanceof \DS\StringTransform? "'": '')."}": '')."),";
				}
			}
			
			$js .= '});';
		}
		return $js;
		
	}
}
?>