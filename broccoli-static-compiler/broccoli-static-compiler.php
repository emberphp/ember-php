<?php
namespace Broccoli;

class Compiler {
	protected static $package;

	public static function init($package) {
		self::$package = \parse_ini_file($package, true);
	}

	public static function bars() {
		// templates/components/*.hbs
		$components = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'*.hbs'));

		// templates/components/*.hbs
		$templates = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'*.hbs'));

		array_walk($components, function(&$v) {
			$v = '<script type="text/x-handlebars" data-template-name="components/'.basename($v, '.hbs').'">'.file_get_contents($v).'</script>';
		});

		array_walk($templates, function(&$v) {
			$v = '<script type="text/x-handlebars" data-template-name="'.basename($v, '.hbs').'">'.file_get_contents($v).'</script>';
		});

		return implode('', array_merge((array)$components, (array)$templates));
	}

	public static function stylesheets() {
		$files = array_filter(array_map(function($v) {
			return ($v->getExtension() == 'css'? $v->getRealPath(): NULL);
		}, iterator_to_array(new \RecursiveIteratorIterator((new \RecursiveDirectoryIterator(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'stylesheets'.DIRECTORY_SEPARATOR, \RecursiveDirectoryIterator::SKIP_DOTS))))));
		$stylePriorities = (is_array(self::$package['stylePriorities'])? self::$package['stylePriorities']: []);
		array_walk($stylePriorities, function(&$v) {
			$v = rtrim(realpath(self::$package['directories']['bin']), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'stylesheets'.DIRECTORY_SEPARATOR.$v;
		});
		$resp = '';
		array_walk(array_merge(array_intersect($stylePriorities, $files), array_diff($files, $stylePriorities)), function($file, $index) use(&$resp) {
			$resp .= file_get_contents($file);
		});
		if(!empty(self::$package['engines']['css-crush'])) {
			require_once self::$package['engines']['css-crush'];
			$resp = \csscrush_string($resp);
		}
		return $resp;
	}

	public static function javascripts() {
		// vendor/*.js
		$vendorFiles = array_filter(array_map(function($v) {
			return ($v->getExtension() == 'js'? $v->getRealPath(): NULL);
		}, iterator_to_array(new \RecursiveIteratorIterator((new \RecursiveDirectoryIterator(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR, \RecursiveDirectoryIterator::SKIP_DOTS))))));

		$dependencies = (is_array(self::$package['dependencies'])? self::$package['dependencies']: []);
		array_walk($dependencies, function(&$v) {
			$v = rtrim(realpath(self::$package['directories']['bin']), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.$v;
		});

		// javascripts/*.js
		$files = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/lib/*.js
		$libFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/components/*.js
		$componentFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/controllers/*.js
		$controllerFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/helpers/*.js
		$helperFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/models/*.js
		$modelFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/routes/*.js
		$routeFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'routes'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/views/*.js
		$viewFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'*.js'));

		// javascripts/mixins/*.js
		$mixinFiles = array_map(function($v) {
			return realpath($v);
		}, glob(rtrim(self::$package['directories']['bin'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.'mixins'.DIRECTORY_SEPARATOR.'*.js'));

		$javascriptPriorities = (is_array(self::$package['javascriptPriorities'])? self::$package['javascriptPriorities']: []);
		array_walk($javascriptPriorities, function(&$v) {
			$v = rtrim(realpath(self::$package['directories']['bin']), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'javascripts'.DIRECTORY_SEPARATOR.$v;
		});

		$vendors = array_merge(
			array_intersect($dependencies, $vendorFiles),
			array_diff($vendorFiles, $dependencies)
		);

		$javascripts = array_merge(
			(array)$files, (array)$libFiles, (array)$componentFiles, (array)$controllerFiles, (array)$helperFiles, (array)$modelFiles, (array)$routeFiles, (array)$viewFiles, (array)$mixinFiles
		);

		$javascripts = array_merge(
			array_intersect($javascriptPriorities, $javascripts),
			array_diff($javascripts, $javascriptPriorities)
		);

		array_walk($vendors, function(&$v) {
			$v = file_get_contents($v)."\n";
		});

		array_walk($javascripts, function(&$v) {
			if(stripos($v, DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'mixins'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'Component='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'routes'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'Route='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'View='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR) !== false) {
				$v = 'App.'.\Ember\String\classify(basename($v, '.js')).'Controller='.file_get_contents($v);

			} elseif(stripos($v, DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR) !== false) {
				$v = "Em.Handlebars.registerBoundHelper('".basename($v, '.js')."',".file_get_contents($v).');';

			} else {
				$v = file_get_contents($v);
			}
		});

		if(!empty(self::$package['engines']['js-shrink'])) {
			require_once self::$package['engines']['js-shrink'];
			$javascripts = array_map(function($v) {
				return \JShrink\Minifier::minify($v);
			}, (array)$javascripts);
		}

		return implode(';', array_merge((array)$vendors, (array)$javascripts));

		/*
		$resp = '';
		$allPaths = array_merge(
			glob(self::$libs.'*.js'),
			glob(self::$js.'vendor/*.js'),
			glob(self::$js.'*.js')
		);
		$priorityPaths = (array_key_exists('libs', self::$priorities)? self::$priorities['libs']: array());
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$resp) {
			$resp .= file_get_contents($file);
		});
		$resp1 = $resp;
		$resp = '';
		$allPaths = glob(self::$js.'lib/*.js');
		$priorityPaths = (array_key_exists('jslibs', self::$priorities)? self::$priorities['jslibs']: array());
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$resp, &$app) {
			if(strrpos(basename($file), $app.'.', -strlen(basename($file))) !== false) {
				$resp .= basename($file, '.js').'='.file_get_contents($file);
			} else {
				$resp .= file_get_contents($file);
			}
		});
		$GLOBALS[self::$app] = new \Ember\Application;
		array_walk(glob(self::$js.'mixins/*.js'), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'=';
			$resp .= file_get_contents($file);
		});
		array_walk(glob(self::$js.'components/*.js'), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Component=';
			$resp .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'routes/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Route=';
			$resp .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'views/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'View=';
			$resp .= file_get_contents($file);
		});
		$allPaths = glob(self::$js.'controllers/*.js');
		$priorityPaths = array();
		array_walk(array_merge(array_intersect($priorityPaths, $allPaths), array_diff($allPaths, $priorityPaths)), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\n$app.".\Ember\String\classify(basename($file, '.js')).'Controller=';
			$resp .= file_get_contents($file);
		});
		array_walk(glob(self::$js.'helpers/*.js'), function($file, $index) use(&$resp, &$app) {
			$resp .= "\n\nEm.Handlebars.registerBoundHelper('".basename($file, '.js')."',";
			$resp .= file_get_contents($file);
			$resp .= ');';
		});

		return array($resp1, $resp);*/
	}
}

?>
