<?php
namespace Ember\String;
define(__NAMESPACE__.'\\STRING_DASHERIZE_REGEXP', '/[ _]/');
define(__NAMESPACE__.'\\STRING_DECAMELIZE_REGEXP', '/([a-z\d])([A-Z])/');
define(__NAMESPACE__.'\\STRING_CAMELIZE_REGEXP', '/(\-|_|\.|\s)+(.)?/');
define(__NAMESPACE__.'\\STRING_UNDERSCORE_REGEXP_1', '/([a-z\d])([A-Z]+)/');
define(__NAMESPACE__.'\\STRING_UNDERSCORE_REGEXP_2', '/\-|\s+/');

/**
	Splits a string into separate units separated by spaces, eliminating any
	empty strings in the process. This is a convenience method for split that
	is mostly useful when applied to the `String.prototype`.

	```php
	foreach(Ember\String\w("alpha beta gamma") as $key) {
		var_dump($key);
	}

	>	alpha
	>	beta
	>	gamma
	```

	@method w
	@param {String} str The string to split
	@return {Array} split string
	*/
function w($str) {
	return preg_split('/\s+/', $str);
}

/**
	Converts a camelized string into all lower case separated by underscores.

	```php
	Ember\String\decamelize('innerHTML');
	Ember\String\decamelize('action_name');
	Ember\String\decamelize('css-class-name');
	Ember\String\decamelize('my favorite items');

	>	'inner_html'
	>	'action_name'
	>	'css-class-name'
	>	'my favorite items'
	```

	@method decamelize
	@param {String} str The string to decamelize.
	@return {String} the decamelized string.
	*/
function decamelize($str) {
	return strtolower(preg_replace(STRING_DECAMELIZE_REGEXP, '$1_$2', $str));
}

/**
	Replaces underscores, spaces, or camelCase with dashes.

	```javascript
	Ember\String\dasherize('innerHTML');
	Ember\String\dasherize('action_name');
	Ember\String\dasherize('css-class-name');
	Ember\String\dasherize('my favorite items');

	>	'inner-html'
	>	'action-name'
	>	'css-class-name'
	>	'my-favorite-items'
	```

	@method dasherize
	@param {String} str The string to dasherize.
	@return {String} the dasherized string.
	*/
function dasherize($str) {
	return preg_replace(STRING_DASHERIZE_REGEXP, '-', decamelize($str));
}

/**
	Returns the lowerCamelCase form of a string.

	```javascript
	'innerHTML'.camelize();          // 'innerHTML'
	'action_name'.camelize();        // 'actionName'
	'css-class-name'.camelize();     // 'cssClassName'
	'my favorite items'.camelize();  // 'myFavoriteItems'
	'My Favorite Items'.camelize();  // 'myFavoriteItems'
	```

	@method camelize
	@param {String} str The string to camelize.
	@return {String} the camelized string.
	*/
function camelize($str) {
	return preg_replace_callback('/^([A-Z])/', function($matches) {
		$match = $matches[1];
		return strtolower($match);
	}, preg_replace_callback(STRING_CAMELIZE_REGEXP, function($matches) {
		$chr = $matches[2];
		return (!empty($chr)? strtoupper($chr): '');
	}, $str));
}

/**
	Returns the UpperCamelCase form of a string.

	```javascript
	'innerHTML'.classify();          // 'InnerHTML'
	'action_name'.classify();        // 'ActionName'
	'css-class-name'.classify();     // 'CssClassName'
	'my favorite items'.classify();  // 'MyFavoriteItems'
	```

	@method classify
	@param {String} str the string to classify
	@return {String} the classified string
	*/
function classify($str) {
	$parts = explode('.', $str);
	$out = array();
	
	for($i = 0; $i < count($parts); $i++) {
		$camelized = camelize($parts[$i]);
		$out[] = ucfirst($camelized);
	}
	
	return implode('.', $out);
}

/**
	More general than decamelize. Returns the lower\_case\_and\_underscored
	form of a string.

	```javascript
	'innerHTML'.underscore();          // 'inner_html'
	'action_name'.underscore();        // 'action_name'
	'css-class-name'.underscore();     // 'css_class_name'
	'my favorite items'.underscore();  // 'my_favorite_items'
	```

	@method underscore
	@param {String} str The string to underscore.
	@return {String} the underscored string.
	*/
function underscore($str) {
	return strtolower(preg_replace(STRING_UNDERSCORE_REGEXP_2, '_', preg_replace(STRING_UNDERSCORE_REGEXP_1, '$1_$2', $str)));
}

/**
	Returns the Capitalized form of a string

	```javascript
	'innerHTML'.capitalize()         // 'InnerHTML'
	'action_name'.capitalize()       // 'Action_name'
	'css-class-name'.capitalize()    // 'Css-class-name'
	'my favorite items'.capitalize() // 'My favorite items'
	```

	@method capitalize
	@param {String} str The string to capitalize.
	@return {String} The capitalized string.
	*/
function capitalize($str) {
	return ucfirst($str);
}

function humanize($str) {
	return preg_replace_callback('/^\w/', function($matches) {
		return strtoupper($matches[0]);
	}, preg_replace('/_/', ' ', preg_replace('/_id$/', '', $str)));
}


function pluralize($string) {
  $plural = array(
            array( '/(quiz)$/i',               "$1zes"   ),
        array( '/^(ox)$/i',                "$1en"    ),
        array( '/([m|l])ouse$/i',          "$1ice"   ),
        array( '/(matr|vert|ind)ix|ex$/i', "$1ices"  ),
        array( '/(x|ch|ss|sh)$/i',         "$1es"    ),
        array( '/([^aeiouy]|qu)y$/i',      "$1ies"   ),
        array( '/([^aeiouy]|qu)ies$/i',    "$1y"     ),
            array( '/(hive)$/i',               "$1s"     ),
            array( '/(?:([^f])fe|([lr])f)$/i', "$1$2ves" ),
            array( '/sis$/i',                  "ses"     ),
            array( '/([ti])um$/i',             "$1a"     ),
            array( '/(buffal|tomat)o$/i',      "$1oes"   ),
            array( '/(bu)s$/i',                "$1ses"   ),
            array( '/(alias|status)$/i',       "$1es"    ),
            array( '/(octop|vir)us$/i',        "$1i"     ),
            array( '/(ax|test)is$/i',          "$1es"    ),
            array( '/s$/i',                    "s"       ),
            array( '/$/',                      "s"       )
        );

        $irregular = array(
        array( 'move',   'moves'    ),
        array( 'sex',    'sexes'    ),
        array( 'child',  'children' ),
        array( 'man',    'men'      ),
        array( 'person', 'people'   )
        );

        $uncountable = array( 
        'sheep', 
        'fish',
        'series',
        'species',
        'money',
        'rice',
        'information',
        'equipment'
        );

        // save some time in the case that singular and plural are the same
        if ( in_array( strtolower( $string ), $uncountable ) )
        return $string;

        // check for irregular singular forms
        foreach ( $irregular as $noun )
        {
        if ( strtolower( $string ) == $noun[0] )
            return $noun[1];
        }

        // check for matches using regular expressions
        foreach ( $plural as $pattern )
        {
        if ( preg_match( $pattern[0], $string ) )
            return preg_replace( $pattern[0], $pattern[1], $string );
        }
    
        return $string;
}

function singularize($params) {
	if (is_string($params))
        {
                $word = $params;
        }
        else if (!$word = $params['word'])
        {
                return false;
        }
        
        $singular = array (
                '/(quiz)zes$/i' => '\\1',
                '/(matr)ices$/i' => '\\1ix',
                '/(vert|ind)ices$/i' => '\\1ex',
                '/^(ox)en/i' => '\\1',
                '/(alias|status)es$/i' => '\\1',
                '/([octop|vir])i$/i' => '\\1us',
                '/(cris|ax|test)es$/i' => '\\1is',
                '/(shoe)s$/i' => '\\1',
                '/(o)es$/i' => '\\1',
                '/(bus)es$/i' => '\\1',
                '/([m|l])ice$/i' => '\\1ouse',
                '/(x|ch|ss|sh)es$/i' => '\\1',
                '/(m)ovies$/i' => '\\1ovie',
                '/(s)eries$/i' => '\\1eries',
                '/([^aeiouy]|qu)ies$/i' => '\\1y',
                '/([lr])ves$/i' => '\\1f',
                '/(tive)s$/i' => '\\1',
                '/(hive)s$/i' => '\\1',
                '/([^f])ves$/i' => '\\1fe',
                '/(^analy)ses$/i' => '\\1sis',
                '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\\1\\2sis',
                '/([ti])a$/i' => '\\1um',
                '/(n)ews$/i' => '\\1ews',
                '/s$/i' => ''
        );
        
        $irregular = array(
                'person' => 'people',
                'man' => 'men',
                'child' => 'children',
                'sex' => 'sexes',
                'move' => 'moves'
        );        

        $ignore = array(
                'equipment',
                'information',
                'rice',
                'money',
                'species',
                'series',
                'fish',
                'sheep',
                'press',
                'sms',
        );

        $lower_word = strtolower($word);
        foreach ($ignore as $ignore_word)
        {
                if (substr($lower_word, (-1 * strlen($ignore_word))) == $ignore_word)
                {
                        return $word;
                }
        }

        foreach ($irregular as $singular_word => $plural_word)
        {
                if (preg_match('/('.$plural_word.')$/i', $word, $arr))
                {
                        return preg_replace('/('.$plural_word.')$/i', substr($arr[0],0,1).substr($singular_word,1), $word);
                }
        }

        foreach ($singular as $rule => $replacement)
        {
                if (preg_match($rule, $word))
                {
                        return preg_replace($rule, $replacement, $word);
                }
        }

        return $word;
}

?>
