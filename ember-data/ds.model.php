<?php
namespace DS;
/**
	* The model class that all Ember Data records descend from.
	*/
class Model extends \Ember\Object {
	public $attributes = [];
	
	/**
		Creates a new model
		*/
	public static function extend($attributes) {
		$instance = new self;
		foreach($attributes as $attributeKey => $attributeVal) {
			if(is_string($attributeVal)) {
				$instance->attributes[$attributeVal] = new Transform;
			} else {
				$instance->attributes[$attributeKey] = $attributeVal;
			}
		}
		return $instance;
	}
	
	private function reflectParser($mysqli_object, $additional_parser) {
		$attributes = [];
		foreach($this->attributes as $property => $bindingOrTransform) {
			if($bindingOrTransform instanceof BelongsToRelationship) {
				$attributes[$property] = $mysqli_object->{(!empty($bindingOrTransform->db)? $bindingOrTransform->db: 'id')};
			} elseif($bindingOrTransform instanceof Transform) {
				$attributes[$property] = $bindingOrTransform->serialize((!empty($bindingOrTransform->db)? $mysqli_object->{$bindingOrTransform->db}: $mysqli_object->{$property}));
			}
		}
		if(!empty($additional_parser)) {
			$attributes = array_merge($attributes, $additional_parser($mysqli_object));
		}
		return $attributes;
	}

	/**
		Coverts an (mysqli)object to an ready-for-json-serialiation array.
		> $json['discounts'] = $App->Discount->reflect($sql1->fetch_all_objects());
		*/
	public function reflect($mysqli_objects, $additional_parser = NULL) {
		if(is_array($mysqli_objects)) {
			$attributes2 = [];
			foreach($mysqli_objects as $mysqli_object) {
				$attributes2[] = $this->reflectParser($mysqli_object, $additional_parser);
			}
			return $attributes2;
		} else {
			return $this->reflectParser($mysqli_objects, $additional_parser);
		}
	}
	
	
	
	
	
	/**
		validates deserialzed json object against model
		*/
	public function validate($deserialized_object) {
		$errors = array();
		
		foreach($this->attributes as $property => $attributeVal) {
			if($attributeVal instanceof Transform) {
				$transform = $attributeVal;
				
				if(!isset($deserialized_object->{$property})) {
					continue;
				}
				
				try {
					if($transform instanceof StringTransform) {
						if(!empty($transform->min)) {
							if(mb_strlen($deserialized_object->{$property}, 'utf-8') < $transform->min) {
								throw new ValidationError($property, (!empty($transform->minMsg)? $transform->minMsg: 'is invalid, minimum length'));
							}
						}
						if(!empty($transform->max)) {
							if(mb_strlen($deserialized_object->{$property}, 'utf-8') > $transform->max) {
								throw new ValidationError($property, (!empty($transform->maxMsg)? $transform->maxMsg: 'is invalid, maximum length'));
							}
						}
					} elseif($transform instanceof NumberTransform) {
						if(!empty($transform->min)) {
							if($deserialized_object->{$property} < $transform->min) {
								throw new ValidationError($property, (!empty($transform->minMsg)? $transform->minMsg: 'is invalid, minimum value'));
							}
						}
						if(!empty($transform->max)) {
							if($deserialized_object->{$property} > $transform->max) {
								throw new ValidationError($property, (!empty($transform->maxMsg)? $transform->maxMsg: 'is invalid, maximum value'));
							}
						}
					}
				} catch(ValidationError $e) {
					$errors[$e->getModelProperty()][] = $e->getMessage();
				}
			}
		}
		
		return $errors;
	}
	
	/**
		transforms a VALIDATED(above) deserialized json object to a database object(create or update)
		*/
	public function db($deserialized_object) {
		$mysqli_object = array();
		
		foreach($this->attributes as $property => $attributeVal) {
			if($attributeVal instanceof Transform) {
				$transform = $attributeVal;
				
				if($transform->noupdatecreate) {
					continue;
				}
				
				if(empty($transform->db)) {
					continue;
				}
				
				$mysqli_object[$transform->db] = $transform->deserialize($deserialized_object->{$property});
				//$newValue = $mysqli_object[$transform->db] = $transform->deserialize($deserialized_object->{$property}); WTF
			}
		}
		
		return $mysqli_object;
	}
}

?>
